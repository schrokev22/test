package com.kevin.springmysql.controller;

import com.kevin.springmysql.model.MetaData;
import com.kevin.springmysql.model.Product;
import com.kevin.springmysql.repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/products"})
public class ProductController {

    private ProductRepository repository;

    ProductController(ProductRepository productRepository) {
        this.repository = productRepository;
    }

    // Retrieve All Products (GET /products)
    @GetMapping
    public List findAll(){
        return repository.findAll();
    }

    //  Retrieve a Product by ID (GET /products/{id})
    @GetMapping(path = {"/{id}"})
    public ResponseEntity<Product> findById(@PathVariable int id){
        return repository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    // Create a new Product (POST /products)
    @PostMapping
    public Product create(@RequestBody Product product){
        if(product.getMetaData() != null){
            product.getMetaData().setProduct(product);
        }
        if(product.getPricingInformation() != null){
            product.getPricingInformation().setProduct(product);
        }
        if(product.getProductDescription() != null){
            product.getProductDescription().setProduct(product);
        }
        return repository.save(product);
    }

    // Update a Product (PUT /products)
    @PutMapping(value="/{id}")
    public ResponseEntity<Product> update(@PathVariable("id") int id,
                                          @RequestBody Product product){
        System.out.println("PRODUCT " + product);
        return repository.findById(id)
                .map(record -> {
                    record.setName(product.getName());
                    record.setModelNumber(product.getModelNumber());
                    record.setProductType(product.getProductType());
                    if(product.getMetaData() != null){
                        record.updateMetaDataAux(product.getMetaData());
                    }
                    if(product.getPricingInformation() != null){
                        record.updatePricingInformation(product.getPricingInformation());
                    }
                    if(product.getProductDescription() != null){
                        record.updateProductDescription(product.getProductDescription());
                    }
                    Product updated = repository.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    // Remove a Product (DELETE /products/{id})
    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<?> delete(@PathVariable("id") int id) {
        return repository.findById(id)
                .map(record -> {
                    repository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }
}