package com.kevin.springmysql.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "product")
public class Product {

    @Id
    private int id;

    private String name;

    @JsonProperty("model_number")
    private String modelNumber;

    @JsonProperty("product_type")
    private String productType;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    @JsonProperty("meta_data")
    @JsonIgnoreProperties("product")
    private MetaData metaData;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    @JsonProperty("pricing_information")
    @JsonIgnoreProperties("product")
    private PricingInformation pricingInformation;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    @JsonProperty("product_description")
    @JsonIgnoreProperties("product")
    private ProductDescription productDescription;

    public Product(){}

    public Product(int id, String name, String modelNumber, String productType,
                   MetaData metaData, PricingInformation pricingInformation,
                   ProductDescription productDescription) {
        this.id = id;
        this.name = name;
        this.modelNumber = modelNumber;
        this.productType = productType;
        this.metaData = metaData;
        this.metaData.setProduct(this);
        this.pricingInformation = pricingInformation;
        this.pricingInformation.setProduct(this);
        this.productDescription = productDescription;
        this.productDescription.setProduct(this);
    }

    public void updateMetaDataAux(MetaData metaData){
        this.metaData.setPageTitle(metaData.getPageTitle());
        this.metaData.setCanonical(metaData.getCanonical());
        this.metaData.setDescription(metaData.getDescription());
        this.metaData.setSiteName(metaData.getSiteName());
        this.metaData.setKeywords(metaData.getKeywords());
    }

    public void updatePricingInformation(PricingInformation pricingInformation){
        this.pricingInformation.setCurrentPrice(pricingInformation.getCurrentPrice());
        this.pricingInformation.setStandardPriceNoVat(pricingInformation.getStandardPriceNoVat());
        this.pricingInformation.setStandardPrice(pricingInformation.getStandardPrice());
    }

    public void updateProductDescription(ProductDescription productDescription){
        this.productDescription.setSubtitle(productDescription.getSubtitle());
        this.productDescription.setText(productDescription.getText());
        this.productDescription.setTitle(productDescription.getTitle());
    }
}