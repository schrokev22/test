package com.kevin.springmysql.model;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name="pricing_information")
public class PricingInformation {

    @Id
    @JsonIgnore
    private int id;

    @OneToOne
    @JoinColumn
    @MapsId
    private Product product;

    @JsonProperty("standard_price")
    private Double standardPrice;
    @JsonProperty("standard_price_no_vat")
    private Double standardPriceNoVat;
    @JsonProperty("current_price")
    private Double currentPrice;

    public PricingInformation(Double standardPrice, Double standardPriceNoVat, Double currentPrice){
        this.standardPrice = standardPrice;
        this.standardPriceNoVat = standardPriceNoVat;
        this.currentPrice = currentPrice;
    }


}