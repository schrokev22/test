package com.kevin.springmysql.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name="meta_data")
public class MetaData {

    @Id
    @JsonIgnore
    private int id;

    @OneToOne
    @JoinColumn
    @MapsId
    private Product product;

    @JsonProperty("page_title")
    private String pageTitle;

    @JsonProperty("site_name")
    private String siteName;

    @JsonProperty("description")
    private String description;

    @JsonProperty("keywords")
    private String keywords;

    @JsonProperty("canonical")
    private String canonical;

    public MetaData(String pageTitle, String siteName, String description, String keywords, String canonical) {
        this.pageTitle = pageTitle;
        this.siteName = siteName;
        this.description = description;
        this.keywords = keywords;
        this.canonical = canonical;
    }
}
