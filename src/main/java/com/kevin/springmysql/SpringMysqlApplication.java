package com.kevin.springmysql;

import com.kevin.springmysql.model.MetaData;
import com.kevin.springmysql.model.PricingInformation;
import com.kevin.springmysql.model.Product;
import com.kevin.springmysql.model.ProductDescription;
import com.kevin.springmysql.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication
public class SpringMysqlApplication implements CommandLineRunner{

	private final ProductRepository productRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringMysqlApplication.class, args);
	}

	@Override
	public void run(String... args) {
		productRepository.deleteAll();
		// Create a couple of products
		productRepository.save(new Product(101, "Nite Jogger Shoes", "BTO93",
				"inline", new MetaData("adidas Nite Jogger Shoes - Black | adidas UK",
				"adidas United Kingdom", "Shop for Nite Jogger Shoes - Black at adidas.co.uk!",
				"Nite Jogger Shoes", "//www.adidas.co.uk/nite-jogger-shoes/CG7088.html"),
				new PricingInformation(119.95, 99.96, 119.95),
				new ProductDescription("Nite Jogger Shoes", "Modern cushioning updates this flashy '80s standout.",
						"Inspired by the 1980 Nite Jogger, these shoes shine bright with retro style")));
		productRepository.save(new Product(102,"Pure Boost Shoes", "BTO95",
				"inline", new MetaData("adidas Pure Boost Shoes - White | adidas DE",
				"adidas Deutschland", "Shop for Pure Boost Shoes - Black at adidas.de!",
				"Pure Boost Shoes", "//www.adidas.de/pure-boost-shoes/CG7090.html"),
				new PricingInformation(139.95, 109.96, 139.95),
				new ProductDescription("Pure Boost Shoes", "Best cushioning of the market.",
						"Inspired by the Boost technology from 2000's")));
	}
}