README

Spring Boot + MySQL + Gradle were used for the implementation

How to run the app
Once the project is cloned from Bitbucket, open command line, go the root folder of the project
Using Gradle, the app can be run with the command: gradle bootRun
Other option is to run the executable jar. Again at the root folder of the project, with the 
following command: java -jar classes/artifacts/springmysql_jar/springmysql.jar

Once the app is running, here are the possible commands
// Retrieve All Products (GET /products)
http://localhost:8080/products/

//  Retrieve a Product by ID (GET /products/{id})
http://localhost:8080/products/101

// Create a new Product (POST /products), with the corresponding data  

// Update a Product (PUT /products)

// Remove a Product (DELETE /products/{id})


Lombok annotation was used to help us keep our classes cleaner without the getters and setters:

Initializing the MySQL database
Two records are inserted in the MySQL product table

Configuring MySQL Database
The connection details to the database are inside src/maind/resources/application.properties:

For production, one might want to create a functional ID (user/password) that can perform all CRUD 
operations in the tables, but cannot perform any DDL commands (Create, Drop, Alter tables, and so on).

After the database is in production state, spring.jpa.hibernate.ddl-auto=create is changed to none to
revoke all privileges from the MySQL user connected to the Spring application, 
then give him only SELECT, UPDATE, INSERT, DELETE.